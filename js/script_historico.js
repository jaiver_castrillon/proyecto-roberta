$( document ).ready(function() {

	//Cargar información de sensores.
	$.ajax({
        url:   '../../Consultas.php',
        type:  'get',
        data: "ACTION=historico",
        success:  function (response) {
            //alert(response);
            var data = JSON.parse(response);
            console.log(data);
            //alert(data['datos'][0]['identificador']);
            agregarDatos(data['datos']);
        }
    });


});


function agregarDatos(datos) {
    $("#dataTable tbody tr" ).each( function(){ this.parentNode.removeChild( this ); }); 
	datos.forEach(function(element) {
		agregarFilaTablaDatos(element['fecha'], element['dato'], element['sensor']);
	});

}

function agregarFilaTablaDatos(fecha, dato, sensor) {
   
   var htmlTags = '<tr>'+
        '<td>' + fecha + '</td>'+
        '<td>' + dato + '</td>'+
        '<td>' + sensor + '</td>'+
      '</tr>';
      
   $('#dataTable tbody').append(htmlTags);

}