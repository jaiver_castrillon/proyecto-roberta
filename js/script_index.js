$( document ).ready(function() {

	//Cargar información de sensores.
	$.ajax({
        url:   '../../Consultas.php',
        type:  'get',
        data: "ACTION=sensores",
        success:  function (response) {
            //alert(response);
            var data = JSON.parse(response);
            console.log(data);
            //alert(data['datos'][0]['identificador']);
            agregarSensores(data['datos']);
        }
    });


});


function agregarSensores(datos) {
	datos.forEach(function(element) {
		agregarFilaTablaSensores(element['identificador'], element['frecuencia'], element['tipo']);
	});

}

function agregarFilaTablaSensores(identificador, frecuencia, tipo) {
   
   var htmlTags = '<tr>'+
        '<td>' + identificador + '</td>'+
        '<td>' + frecuencia + '</td>'+
        '<td>' + tipo + '</td>'+
      '</tr>';
      
   $('#dataTable tbody').append(htmlTags);

}